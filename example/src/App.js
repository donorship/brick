import React from 'react'

import 'brick/dist/index.css'

import { Button } from 'brick'

const App = () => {
  return <Button variant='primary'>Ciao</Button>
}

export default App
