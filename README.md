# brick

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/brick.svg)](https://www.npmjs.com/package/brick) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save brick
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'brick'
import 'brick/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [](https://github.com/)
